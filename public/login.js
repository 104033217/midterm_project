function initLogin() {
  const fb = firebase;
  let [ useremailInput, usernameInput, passwordInput ] = document.querySelectorAll(".login-input input");
  let [ registerBtn, loginBtn ] = document.querySelectorAll(".login-btn button");
  const usersRef = fb.database().ref("users");

  loginBtn.addEventListener('click', function () {
    let [useremail, password] = [
      useremailInput.value, 
      passwordInput.value
    ];
    fb.auth().signInWithEmailAndPassword(useremail, password)
    .then((m) => {
      console.log(m);
      window.location.href = window.location.origin + "";

    })
    .catch((err) => {
      alert([err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣"));
    });
  });

  registerBtn.addEventListener('click', async function () {
    let [useremail, username, password] = [
      useremailInput.value, 
      usernameInput.value, 
      passwordInput.value
    ];
    if(!username){
      alert("這裡不是匿名討論區哦～");
      return;
    }
    
    // 檢查同名(username)使用者
    let sameUsername = false;
    // await and check sameUsername
    await usersRef.once("value")
    .then((users) => {
      if(users.val()){
        for( let user of Object.values(users.val()) ){
          if(user.username === username){
            sameUsername = true;
            break;
          }
        }
      }
    })
    .catch((err) => {
      alert([err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ once value fail"));
    });
    if(sameUsername) {
      alert("這個使用者名稱已經被塞爆");
      return;
    }

    // new user
    fb.auth().createUserWithEmailAndPassword(useremail, password)
    .then((m) => {
      let newUser = usersRef.push();
      newUser.set({
        useremail: useremail,
        username: username,
        post_count: 0
      });
      window.location.href = window.location.origin + "";

    })
    .catch((err) => {
      alert([err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ layer1"));
    })
  });

  // login or signup 選擇
  let op_login_btn = document.querySelector(".op-login");
  let op_signup_btn = document.querySelector(".op-signup");
  op_login_btn.addEventListener("click", () => {
    loginBtn.style.display = "block";
    registerBtn.style.display = "none";
    op_login_btn.style.display = "none";
    op_signup_btn.style.display = "block";
    usernameInput.style.display = "none";
  });
  op_signup_btn.addEventListener("click", () => {
    loginBtn.style.display = "none";
    registerBtn.style.display = "block";
    op_login_btn.style.display = "block";
    op_signup_btn.style.display = "none";
    usernameInput.style.display = "block";
  });
}


window.onload = function () {
  initLogin();
  firebase = null;
};