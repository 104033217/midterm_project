async function initForum(){
  const fb = firebase;
  let postsRef = fb.database().ref("posts");
  let postsFound = [];
  let forumMain = document.querySelector(".forum-main");
  let forumName = forumMain.dataset.forum;
  let articleList = document.querySelector(".article-list");
  let pageNavigator = document.querySelectorAll(".page-navigator")[0];
  let page = pageNavigator.dataset.page;

  await postsRef.once("value")
  .then((posts) => {
    for( let post of Object.values(posts.val()) ){
      if(post.forum === forumName){
        postsFound.push(post);
      }
    }
  })
  .catch((err) => {
    alert([err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ once value fail"));
  });


  // n articles in one page
  // 建置page navigator
  let n = 10;
  let pageNumber = postsFound.length / n;
  if(postsFound.length % n != 0) pageNumber++;
  let pageNavString = "";
  for(let i = 1; i <= pageNumber; i++){
    if(i === parseInt(page) ){
      pageNavString += `<div class="nowPage page-n">${i}</div>`;
    }
    else {
      pageNavString += `<div class="page-n">${i}</div>`;
    }
  }
  pageNavigator.innerHTML = pageNavString;
  showPost();
  loadingOverlay("hidden");
  
  // 換頁的code
  let currentPageN = document.querySelectorAll(".nowPage")[0];
  function pageN(e){
    if(!e.target.classList.contains("page-n")) return;
    currentPageN.classList.remove("nowPage");
    e.target.classList.add("nowPage");
    currentPageN = e.target;
    pageNavigator.dataset.page = e.target.textContent;
    window.history.pushState(null, null, `/forum/${forumName}/${e.target.textContent}`);
    showPost();
  }
  pageNavigator.addEventListener("click", pageN);

  function showPost(){
    // clear list
    while(articleList.firstChild){
      articleList.removeChild(articleList.firstChild);
    }

    // refetch page number
    let page = pageNavigator.dataset.page;

    let articleTemplate = document.querySelector(".article-template");
    let start = postsFound.length - 1 - (page - 1) * n;
    let end = Math.max(start - (n - 1), 0);
    for(let i = start; i >= end; i--){
      let item = document.importNode(articleTemplate.content, true);
      p = postsFound[i];
      title = item.querySelector(".post-title a");
      title.textContent = p.title;
      title.href = `/post/${p.id}`;
      let author = item.querySelector(".post-author");
      let authorLink = author.querySelector("a");
      authorLink.textContent = p.author;
      author.innerHtml += " posted at ";
      authorLink.href = "/user/" + p.author;
      item.querySelector(".post-time").textContent = `${p.time.year}/${p.time.month}/${p.time.date} ${p.time.hour}:${p.time.minute}:${p.time.second}`;
      item.querySelector(".post-ninnki").textContent = p.ninnki;
      articleList.appendChild(item);
    }
  }
}

window.onload = async function(){
  initNavbar();
  initForum();
  firebase = null;
}