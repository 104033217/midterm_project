function initNewPost() {
  const fb = firebase;
  let postsRef = fb.database().ref("posts");
  let usersRef = fb.database().ref("users");
  let postsLogRef = fb.database().ref("postsLog");
  let post_title = document.querySelector(".newpost-title input");
  let post_content = document.querySelector(".newpost-content textarea");
  let post_btn = document.querySelector(".newpost-op button");

  let forum = post_btn.dataset.forum;
  post_btn.textContent = "發文至看板 " + forum;
  loadingOverlay("hidden");

  post_btn.addEventListener("click", async () => {
    // check state
    let currentUser = fb.auth().currentUser;
    if(!currentUser){
      alert("你是不是沒登入？");
      return;
    }
    if(post_title.value === ""){
      alert("沒標題還想發文啊");
      return;
    }
    if(post_content.value === ""){
      alert("內容空泛可是要被桶的");
      return;
    }

    // prepare post data
    let date = new Date();
    let time = {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      date: date.getDate(),
      hour: date.getHours(),
      minute: date.getMinutes(),
      second: date.getSeconds()
    }
    let currentUserData;
    let uname;
    await usersRef.orderByChild("useremail").equalTo(currentUser.email).once("value")
    .then((user) => {
      currentUserData = Object.values(user.val())[0];
      uname = currentUserData.username;
    })
    .catch((err) => {
      alert([err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ once value fail"));
    });

    let id;
    await postsLogRef.once("value")
    .then((postsLog) => {
      // 最初的貼文
      if(!postsLog.val()){
        postsLogRef.set({
          number: 0
        })
        id = 0;
      }
      else {
        let pl = postsLog.val();
        pl.number += 1;
        postsLogRef.update(pl);
        id = pl.number;
      }
    })
    .catch((err) => {
      alert([err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ once value fail"));
    });

    await postsRef.once("value")
    .then((posts) => {
      if(posts.val()){
        postsNumber = Object.keys(posts.val()).length;
      }
    })
    .catch((err) => {
      alert([err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ once value fail"));
    });
    

    // insert db data
    if(uname){
      let newPost = postsRef.push();
      newPost.set({
        id,
        author: uname,
        title: post_title.value,
        content: post_content.value.split("\n"),
        reply: [],
        time,
        forum,
        ninnki: 0
      });
      window.location.href = window.location.origin + "/forum/" + forum;
    } else {
      alert(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ user not found");
    }

  });

}

window.onload = function(){
  initNavbar();
  initNewPost();
  firebase = null;
};