async function initUserPage(){
  const fb = firebase;
  let postsRef = fb.database().ref("posts");
  let usersRef = fb.database().ref("users");
  let postsFound = [];
  let userMain = document.querySelector(".user-main");
  let userName = userMain.dataset.user;
  let articleList = document.querySelector(".article-list");
  let pageNavigator = document.querySelectorAll(".page-navigator")[0];
  let page = pageNavigator.dataset.page;

  // load user data
  await usersRef.orderByChild("username").equalTo(userName).once("value")
  .then(user => {
    console.log(userName);
    document.querySelector(".user-email").textContent = Object.values(user.val())[0].useremail;
  })
  .catch((err) => {
    alert([err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ once value fail"));
  });

  await postsRef.orderByChild("author").equalTo(userName).once("value")
  .then((posts) => {
    if(posts.val()){
      postsFound = Object.values(posts.val());
    }
  })
  .catch((err) => {
    alert([err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ once value fail"));
  });

  // n articles in one page
  let n = 6;
  let pageNumber = postsFound.length / n;
  if(postsFound.length % n != 0) pageNumber++;
  let pageNavString = "";
  for(let i = 1; i <= pageNumber; i++){
    if(i === parseInt(page) ){
      pageNavString += `<div class="nowPage page-n">${i}</div>`;
    }
    else {
      pageNavString += `<div class="page-n">${i}</div>`;
    }
  }
  pageNavigator.innerHTML = pageNavString;
  let currentPageN = document.querySelectorAll(".nowPage")[0];
  function pageN(e){
    if(!e.target.classList.contains("page-n")) return;
    currentPageN.classList.remove("nowPage");
    e.target.classList.add("nowPage");
    currentPageN = e.target;
    pageNavigator.dataset.page = e.target.textContent;
    showPost();
  }
  pageNavigator.addEventListener("click", pageN);

  showPost();

  loadingOverlay("hidden");

  function showPost(){
    // stackoverflow
    while(articleList.firstChild){
      articleList.removeChild(articleList.firstChild);
    }
    //........................

    let page = pageNavigator.dataset.page;
    let articleTemplate = document.querySelector(".article-template");
    let start = postsFound.length - 1 - (page - 1) * n;
    let end = Math.max(start - (n - 1), 0);
    for(let i = start; i >= end; i--){
      let item = document.importNode(articleTemplate.content, true);
      p = postsFound[i];
      title = item.querySelector(".post-title a");
      title.textContent = p.title;
      title.href = `/post/${p.id}`;
      item.querySelector(".post-time").textContent = `${p.time.year}/${p.time.month}/${p.time.date} ${p.time.hour}:${p.time.minute}:${p.time.second}`;
      item.querySelector(".post-ninnki").textContent = p.ninnki;
      item.querySelector(".post-forum").textContent = p.forum;
      articleList.appendChild(item);
    }
  }
}

window.onload = function(){
  initNavbar();
  initUserPage();
  firebase = null;
}