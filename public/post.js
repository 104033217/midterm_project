async function initPost() {
  const fb = firebase;
  let postsRef = fb.database().ref("posts");
  let postMain = document.querySelector(".post-main");
  let usersRef = fb.database().ref("users");

  let post;
  await postsRef.once("value")
  .then((posts) => {
    // note difference between for of and for in 
    for(let p of Object.values(posts.val()) ){
      if(p.id === parseInt(postMain.dataset.post_id)){
        post = p;
      }
    }
  })
  .catch((err) => {
    alert([err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ once value fail"));
  });

  // 載入文章內容
  if(post){
    postMain.querySelector(".post-title").textContent = "標題 " + post.title;
    let author = postMain.querySelector(".post-author");
    let authorLink = document.createElement("a")
    authorLink.textContent = post.author;
    authorLink.href = "/user/" + post.author;
    author.textContent = "作者 ";
    author.appendChild(authorLink);
    postMain.querySelector(".post-time").textContent = `${post.time.year}/${post.time.month}/${post.time.date} ${post.time.hour}:${post.time.minute}:${post.time.second}`;
    postMain.querySelector(".post-forum").textContent = "看板 " + post.forum;

    let content = postMain.querySelector(".post-content");
    post.content.forEach((sentence) => {
      let m = document.createElement("div");
      // 空行和空格處理
      m.textContent = sentence.replace(/ /g, "\xa0");
      if(sentence === "") {
        m.appendChild(document.createElement("br"));
      }
      content.appendChild(m);
    })

    let replyTemplate = document.querySelector(".reply-template");
    let replyList = postMain.querySelector(".post-reply-list");
    if(post.reply){
      post.reply.forEach((reply) => {
        let r = document.importNode(replyTemplate.content, true);
        r.querySelector(".reply-author").textContent = reply.author;
        r.querySelector(".reply-time").textContent = `${reply.time.year}/${reply.time.month}/${reply.time.date} ${reply.time.hour}:${reply.time.minute}:${reply.time.second}`;
        r.querySelector(".reply-content").textContent = reply.content.replace(/ /g, "\xa0");
        replyList.appendChild(r);
      });
    }

  } else {
    console.log("沒找到文章歐");
  }
  
  loadingOverlay("hidden");

  let replyNew = document.querySelector(".reply-new");
  let replyBtn = replyNew.querySelector("button");
  let replyText = replyNew.querySelector("input");

  replyBtn.addEventListener("click", async () => {
    // check state
    let currentUser = fb.auth().currentUser;
    if(!currentUser){
      alert("你是不是沒登入？");
      return;
    }
    if(replyText.value === ""){
      alert("沒內文喔");
      return;
    }

    let date = new Date();
    let time = {
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      date: date.getDate(),
      hour: date.getHours(),
      minute: date.getMinutes(),
      second: date.getSeconds()
    };
    let uname;
    await usersRef.once("value")
    .then((users) => {
      if(users.val()){
        for( let user of Object.values(users.val()) ){
          if(user.useremail === currentUser.email){
            uname = user.username;
            break;
          }
        }
      }
    })
    .catch((err) => {
      alert([err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ once value fail"));
    });

    let newReply = {
      author: uname,
      time,
      content: replyText.value
    };

    let updatePostRef = postsRef.orderByChild("id").equalTo(parseInt(postMain.dataset.post_id));
    await updatePostRef.once("value")
    .then((updatePost) => {
      let hash = Object.keys(updatePost.val())[0];
      let p = Object.values(updatePost.val())[0];
      if(!p.reply){
        p.reply = [newReply];
        p.ninnki = 1;
      } else {
        p.reply.push(newReply);
        p.ninnki++;
      }
      fb.database().ref("posts/" + hash).update(p);
      replyText.value = "";
      alert("回覆成功，但是開發者很懶，你要自己F5");
    })
    .catch((err) => {
      alert([err.code, err.message].join(" ◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣ once value fail"));
    });
  
  });

}

window.onload = function(){
  initNavbar();
  initPost();
  firebase = null;
};