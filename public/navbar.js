
async function initNavbar(){
  let fb = firebase;
  let navbar = document.querySelector(".navbar");
  let navbarUserOption = navbar.querySelector(".nav-user-l");
  let navbarUser = navbar.querySelector(".nav-user");
  let uuuuser;
  
  // 根據登入狀態產生不同的使用者選單
  fb.auth().onAuthStateChanged(async (user) => {
    if(!user){
      navbarUserOption.innerHTML = `
        <div>
          <a href=\"/login\">登入！</a>
        </div>
      `;
    }
    else {
      let usersRef = fb.database().ref("users")
      await usersRef.orderByChild("useremail").equalTo(user.email).once("value")
      .then((u) => {
        uuuuser = Object.values(u.val())[0];
      })
      .catch((err) => {
        window.alert(err + "◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣");
      });

      navbarUserOption.innerHTML = `
        <div class="toUserPage">${uuuuser.username}的主頁</div>
        <div>更新</div>
        <div class="logout">登出！</div>
      `;

    }

    let logoutBtn = document.querySelector(".logout");
    if(logoutBtn){
      logoutBtn.addEventListener("click", () => {
        fb.auth().signOut()
        .then(() => {
          window.location.href = window.location.origin;
        })
        .catch((err) => {
          window.alert(err + "◢▆▅▄▃ 崩╰(〒皿〒)╯潰 ▃▄▅▆◣");
        });
      });
    }

    let userPageBtn = document.querySelector(".toUserPage");
    if(userPageBtn){
      userPageBtn.addEventListener("click", () => {
        window.location.href = window.location.origin + "/user/" + uuuuser.username;
      })
    }

    document.addEventListener("click", (e) => {
      if(e.path.includes(navbarUser)){
        navbarUserOption.style.display = "block";
      }
      else {
        navbarUserOption.style.display = "none";
      }
    });

  });


}



function loadingOverlay(to){
  let loadingOverlay = document.querySelector(".loading-overlay");
  if(to === "show"){
    loadingOverlay.classList.remove("hidden");
  }
  else if (to === "hidden"){
    loadingOverlay.classList.add("hidden");
  }
}