# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer
* remember to run `npm install` in "functions" folder

## Topic
* Project Name : fake forum
* Key functions (add/delete)
  * forum
  * users can:
    * login
    * logout
    * create new post
    * reply on post
    * have personal page
    
* Other functions (add/delete)
  * page navigator

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：https://midp-ee487.firebaseapp.com/
<img src="./2019-05-01_180833.png">

# Components Description : 
- 登入(login page)
    - 登入
        - 輸入信箱、密碼
        - 密碼沒遮，輸入時請注意後面有沒有人(如果你很在意的話)
    - 註冊
        - 信箱、密碼、使用者名稱(不可跟其他使用者重複)
    - 點右下角文字切換 登入/註冊
    - 
- navbar(出現在以下所有頁面)
  - 使用者頭像(沒有實做，只是固定的一張圖)     
    - 沒登入的話只會有個連結讓你去登入
    - 有登入的話會有導向個人頁面以及登出的選項
  - 各個看板連結
    - 八卦版
    - 八卦二版
- 主版面 : 從host進來會自動轉到看板版面 = 主版面
  - 發表文章(link)
  - 看板文章列表
    - 顯示文章基本資訊
      - 標題(link)
      - 作者(link)
      - PO文時間
      - 人氣(回覆數)
- 文章頁面
  - 文章資訊
  - 文章內容
  - 底下回覆
  - 發表回覆
- 發表文章
  - 根據你從哪個看板來，決定文章要發到哪個看板
- 個人頁面(公開)
  - 登入帳號
  - 頭像(沒有實做，只是固定的一張圖)
  - 文章列表顯示這個使用者發表的文章


# Other Functions Description(1~10%) : 
- page navigator
  - 翻頁，方便瀏覽文章列表
  - firebase他...

## Security Report (Optional)
- server端跟database不會記錄使用者密碼，密碼只交給firebase處理，即使database被外界存取也不用擔心帳號安全
- 沒登入不讓你PO文、發表回覆
- 使用者無法在PO文、回覆等地方嵌入html元素
- 使用者無論有沒有登入，都無法「直接」存取你已經設定好的firebase reference
  - 在初始化函數內部另外生一個firebase的reference，把外部firebase變數assign到null
    - `async function initForum(){ const fb = firebase; ......`
- 上述只是防君子不防小人，使用者還是可以拿你的apikey去存取database，然後因為database裡面的PO文資料或額外的使用者資料並沒有檢查Ownership，因此這個網站是
### 極度不安全的
- 很有可能因為某個使用者閒閒沒事修改了你的使用者統計數據或者某篇PO文資訊，導致整個網站資料格式不正確，執行錯誤變得無法顯示任何資訊
- 
- 後來稍微查了一下firebase如何檢查Ownership，實作時還是不太行@@，怪怪der
- 假設我的user data長這樣
```
user們
  |
  -- Ld8Wg5gE3ov3SxjicGM
  |    -- user_id: dddd (這個值來自 fb.auth().currentUser.uid)
  |    -- username: someone1
  |
  -- aabbccddeeffkfsdopf
       -- user_id: ssss
       -- username: someone2
```
然後規則長這樣
```
"user們": {
  "$user_hash": {
    ".read": "data.child('user_id').val() === auth.uid"
  },
}
```
- 使用者someone2的實作結果會是這樣
  - ref("users").once("value") -> have no permission
  - ref("users/Ld8Wg5gE3ov3SxjicGM").once("value") -> have no permission
  - ref("users/aabbccddeeffkfsdopf").once("value") -> ok
  
- 我可以檢查Ownership，但因為我不能看其他user的資料，因此同樣不能取得整個"user們"  
  - 我如果要限制Ownership，例如一個user不能修改其他user的文章，會造成使用者同時不能push新的文章(存取整個posts list)  
  - 我如果要限制Ownership，例如一個user不能修改其他user的使用者資料，會造成無法註冊新帳號(卡在write新的user information到database這個動作)

# resource
- akari.jpg
- noface.gif
  - bilibili
- EmojiOne2.1_fire.png
  - emojione 2.1
  
