let functions = require('firebase-functions');
let express = require("express"),
      app = express(),
      router = express.Router();

let fs = require("fs");
let renderer = require("vue-server-renderer");

app.use("/", router);


router.get("/", (req, res) => {
  res.redirect("/forum/gossiping/1");
});
router.get("/login", (req, res) => {
  let context = {
    title: "new title"
  };
  res.send( renderPage("./login.html", context) );
});

router.get("/newpost", (req, res) => {
  let context = {
    navbar: renderPage("./navbar.html"),
    title: "new post",
    forum: req.query.forum
  };
  res.send( renderPage("./newpost.html", context) );
});

router.get("/post/:id", (req, res) => {
  let context = {
    navbar: renderPage("./navbar.html"),
    postId: req.params.id
  };
  res.send( renderPage("./post.html", context) );
});

router.get("/forum/:forumName/:page", (req, res) => {
  let context = {
    navbar: renderPage("./navbar.html"),
    forum: req.params.forumName,
    page: req.params.page
  };
  res.send( renderPage("./forum.html", context) );
});

router.get("/forum/:forumName", (req, res) => {
  res.redirect(`/forum/${req.params.forumName}/1`);
});

router.get("/user/:userName", (req, res) => {
  let context = {
    navbar: renderPage("./navbar.html"),
    user: req.params.userName
  }
  res.send( renderPage("./user.html", context) );
});



const Vue = require("vue");
function renderPage(view, context){
  let vue_app = new Vue({
    template: `<span></span>`
  });
  let t = renderer.createRenderer({
    template: fs.readFileSync(view, "utf-8")
  });

  let content;
  // only use the compiled html
  t.renderToString(vue_app, context, (err, html) => {
    if(err){
      console.log(err);
      content = "rendering problem";
    }
    else {
      content = html;
    }
  });
  return content;
}

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.app = functions.https.onRequest(app);